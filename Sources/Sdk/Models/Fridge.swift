//
//  File.swift
//  
//
//  Created by Jonas Weigand on 20.10.20.
//

public protocol FridgeProto {
    var name: String { get }
    var items: [FridgeItem] { get }
}
