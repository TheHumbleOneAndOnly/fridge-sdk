//
//  File.swift
//  
//
//  Created by Jonas Weigand on 20.10.20.
//

public protocol FridgeItem {
    var name: String { get }
    var amount: Int { get }
}
